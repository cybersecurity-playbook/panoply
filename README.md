# PANOPLY #



### What is PANOPLY? ###

**PANOPLY** (**P**alo **A**lto **N**etworks **O**rchestration & Integration **PL**a**Y**books) 
is a collection of Cortex XSOAR playbooks that integrate with the following security solutions:  

* **AWS Security Hub**
* **GCP Chronicle**
* **Sixgill**
* **RiskIQ**